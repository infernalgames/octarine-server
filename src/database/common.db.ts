import * as Sequelize from 'sequelize';
import {ModelDefinition} from './core';

export class Patch implements ModelDefinition {
    public tableName = 'patch';
    public attributes = {
        version: {
            type: Sequelize.INTEGER,
        },
    };
}
