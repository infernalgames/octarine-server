import {Models} from 'sequelize';
import * as Sequelize from 'sequelize';
import {ModelDefinition} from './core';

export class TranslateValue implements ModelDefinition {
    public tableName = 'translate_value';

    public attributes = {
        language: {
            type: Sequelize.STRING(2),
        },
        value: {
            type: Sequelize.STRING,
        },
    };

    public config = {
        timestamps: false,
    };

    public assign(models: Models) {
        models.TranslateValue.belongsTo(models.Translate, {foreignKey: {allowNull: false}});
    }
}

export class Translate implements ModelDefinition {
    public tableName = 'translate';

    public config = {
        timestamps: false,
    };

    public scopes(models: Models) {
        return {
            defaultScope: {
                include: [
                    {
                        model: models.TranslateValue,
                        as: 'values',
                    },
                ],
            },
        };
    }

    public assign(models: Models) {
        models.Translate.hasMany(models.TranslateValue, {as: 'values'});
    }
}
