import {Models} from 'sequelize';
import * as Sequelize from 'sequelize';
import {Octarine} from '../octarine';
import {ModelDefinition} from './core';

export class PermissionValue implements ModelDefinition {
    public tableName = 'permission_value';
    public attributes = {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        value: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
    };
    public config = {
        timestamps: false,
        indexes: [
            {
                fields: ['permissionId', 'name'],
                unique: true,
            },
        ],
        defaultScope: {
            attributes: ['id', 'name', 'value'],
        },
    };
}

export class Permission implements ModelDefinition {
    public tableName = 'permission';
    public config = {
        timestamps: false,
    };

    public scopes(models: Models) {
        return {
            defaultScope: {
                include: [
                    {
                        model: models.PermissionValue,
                        as: 'values',
                    },
                ],
            },
        };
    }

    public assign(models: Models) {
        models.Permission.hasMany(models.PermissionValue, { as: 'values', onDelete: 'CASCADE' });
    }
}

export class Role implements ModelDefinition {
    public tableName = 'role';
    public attributes = {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
    };

    public hooks(models: Models) {
        return {
            beforeCreate: async (role: any, options: any) => {
                const permission = await models.Permission.create();
                role.permissionId = permission.id;
                role.permission = permission;
            },
        };
    }

    public scopes(models: Models) {
        return {
            defaultScope: {
                include: [
                    {
                        model: models.Permission,
                    },
                ],
            },
        };
    }

    public assign(models: Models): void {
        models.Role.belongsTo(models.Permission, { onDelete: 'CASCADE' });
    }
}

export class User implements ModelDefinition {
    public tableName = 'user';
    public attributes = {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    };

    public hooks(models: Models) {
        return {
            beforeCreate: async (user: any, options: any) => {
                user.password = Octarine.instance.passwordHash(user.password);
                const permission = await models.Permission.create();
                user.permissionId = permission.id;
                user.permission = permission;
            },
        };
    }

    public scopes(models: Models) {
        return {
            defaultScope: {
                subQuery: false,
                attributes: { exclude: ['password'] },
                include: [
                    {
                        model: models.Permission,
                    },
                    {
                        model: models.Role,
                    },
                ],
            },
            catalog: {
                attributes: ['id', 'username'],
            },
        };
    }

    public assign(models: Models): void {
        models.User.belongsTo(models.Permission, { onDelete: 'CASCADE' });
        models.User.belongsTo(models.Role, { onDelete: 'CASCADE' });
    }
}

export class Token implements ModelDefinition {
    public tableName = 'token';
    public attributes = {
        token: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    };

    public assign(models: Models): void {
        models.Token.belongsTo(models.User, { onDelete: 'CASCADE' });
    }
}
