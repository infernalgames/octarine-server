import * as Sequelize from 'sequelize';
import {Model, Models} from 'sequelize';
import {IDatabaseConfig} from '../config';

export interface ModelDefinition {
    tableName: string;
    attributes?: {[name: string]: any};
    config?: any;
    assign?: (models: Models) => void;
    scopes?: (models: Models, database: any) => {[name: string]: any};
    hooks?: (models: Models) => {[name: string]: any};
}

const buildModel = (sequelize: any, definition: ModelDefinition): Model<any, any> => {
    return sequelize.define(definition.tableName, definition.attributes, definition.config);
};

export const buildModels = (sequilize: any, modelsDefinitions: Array<ModelDefinition>): Models => {
    const models: Models = {};
    for (const modelDefinition of modelsDefinitions) {
        models[modelDefinition.constructor.name] = buildModel(sequilize, modelDefinition);
    }
    for (const modelDefinition of modelsDefinitions) {
        // console.log(`process model: ${modelDefinition.constructor.name}`);
        const model = models[modelDefinition.constructor.name];
        if (modelDefinition.assign) {
            modelDefinition.assign(models);
        }
        if (modelDefinition.scopes) {
            const scopes = modelDefinition.scopes(models, sequilize);
            for (const entry of Object.entries(scopes)) {
                model.addScope(entry[0], entry[1], {
                    override: true,
                });
            }
        }
        if (modelDefinition.hooks) {
            const hooks = modelDefinition.hooks(models);
            for (const entry of Object.entries(hooks)) {
                model.addHook(entry[0], entry[1]);
            }
        }
    }
    return models;
};

export const createDatabase = (config: IDatabaseConfig): any => {
    return new Sequelize(
        {
            host: config.host,
            database: config.database,
            username: config.username,
            password: config.password,
            dialect: config.dialect || 'mysql',
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000,
            },
            // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
            operatorsAliases: false,
            define: {
                charset: 'utf8',
                collate: 'utf8_general_ci',
                timestamps: true,
            },
            logging: config.logging,
        },
    );
};
