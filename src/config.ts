
export interface IDatabaseConfig {
    host?: string;
    database: string;
    username?: string;
    password?: string;
    dialect?: string;
    logging?: boolean;
}

export interface IConfig {
    database: IDatabaseConfig;
    salt: string;
}
