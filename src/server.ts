import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import {Express, NextFunction, Request, Response} from 'express';
import * as express from 'express';
import * as fileUpload from 'express-fileupload';
import {ServletClass} from './api/servlet';

export const createApp = (servlets: Array<ServletClass<any>>): Express => {
    const app: Express = express();
    app.set('json spaces', 2);
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(fileUpload({
        limits: { fileSize: 50 * 1024 * 1024 },
    }));
    for (const servlet of servlets) {
        servlet.bind(app);
    }
    app.use((err: any, request: Request, response: Response, next: NextFunction) => {
        console.error(err);
        response.status(500).send('Internal Server Error');
    });
    return app;
};
