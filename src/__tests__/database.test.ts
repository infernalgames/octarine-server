import {createDatabase} from '../database/core';
import {Octarine} from '../octarine';
import {config} from './config-test';

test('createDatabase', () => {
    expect(createDatabase(config.database)).toBeDefined();
});

test('createRoleAndUser', async () => {
    const octarine = new Octarine(config);
    await octarine.database.sync({force: true});
    const Role = octarine.models.Role;
    const User = octarine.models.User;
    const role = await Role.create({name: 'Test Role'});
    const user = await User.create({username: 'Test User', password: 'test', roleId: role.id});

    const userBase = await User.scope({attributes: ['id', 'username', 'password']}).findByPk(user.id);
    expect(userBase.password).toBe('d00c0d2dd6a5f000a1edaa89c5bd2963');

    const userDefault = await User.scope('defaultScope').findByPk(user.id);
    expect(userDefault.username).toBe(user.username);
    expect(userDefault.password).toBeUndefined();
    expect(userDefault.role.name).toBe(role.name);

    const userCatalog = await User.scope('catalog').findByPk(user.id);
    expect(userCatalog.password).toBeUndefined();
    await octarine.database.close();
});
