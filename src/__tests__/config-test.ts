import {IConfig} from '../config';

export const config: IConfig = {
    database: {
        database: 'test',
        host: '/tmp/test',
        dialect: 'sqlite',
    },
    salt: 'test',
};
