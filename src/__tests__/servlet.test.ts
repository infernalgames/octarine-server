import * as request from 'supertest';
import {Octarine} from '../octarine';
import {config} from './config-test';

const testUser = {
    username: 'test',
    password: 'test',
};

const createOctarine = async (): Promise<Octarine> => {
    const octarine = new Octarine(config);
    await octarine.database.sync({force: true});
    await octarine.models.User.create(testUser);
    return octarine;
};

const auth = async (octarine: Octarine): Promise<string> => {
    const response = await request(octarine.app)
        .post('/auth/login')
        .send(testUser);
    return response.header['set-cookie'][0].split(';')[0];
};

test('auth', async () => {
    const octarine = await createOctarine();
    let response = await request(octarine.app)
        .get('/auth/me');
    expect(response.status).toBe(200);
    expect(response.body.code).toBe(401);

    response = await request(octarine.app)
        .post('/auth/login')
        .send(testUser);

    expect(response.status).toBe(200);
    expect(response.body.code).toBe(200);
    expect(response.header['set-cookie']).toBeDefined();
    const cookie = response.header['set-cookie'][0].split(';')[0];

    response = await request(octarine.app)
        .get('/auth/me')
        .set('Cookie', cookie);
    expect(response.status).toBe(200);
    expect(response.body.code).toBe(200);
    expect(response.body.response.username).toBe(testUser.username);
});

test('user', async () => {
    const octarine = await createOctarine();
    const cookie = await auth(octarine);
    const response = await request(octarine.app)
        .get('/user')
        .set('Cookie', cookie);
    expect(response.status).toBe(200);
    expect(response.body.code).toBe(200);
    expect(response.body.response.data.length).toBe(1);
});
