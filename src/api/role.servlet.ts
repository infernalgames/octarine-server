import {authHandler} from './auth.servlet';
import {ModelServlet} from './model.servlet';
import {servlet} from './servlet';

@servlet('role', authHandler)
export class RoleServlet extends ModelServlet {
    public modelName = 'Role';
}
