import {Request} from 'express';
import {Octarine} from '../octarine';
import {authHandler} from './auth.servlet';
import {ModelServlet} from './model.servlet';
import {ApiError, point, servlet} from './servlet';

@servlet('user', authHandler)
export class UserServlet extends ModelServlet {
    public modelName = 'User';

    @point(':id/password', 'post')
    public async changePassword(request: Request) {
        const model = await this.model.findByPk(request.params.id);
        if (!model) {
            throw ApiError.NotFound;
        }
        await model.update({password: Octarine.instance.passwordHash(request.body.password)});
        return await this.modelToJSON(model);
    }
}
