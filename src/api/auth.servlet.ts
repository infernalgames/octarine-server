import * as crypto from 'crypto';
import {Request, Response} from 'express';
import {Model} from 'sequelize';
import {M} from '../database/bundle';
import {Octarine} from '../octarine';
import {ApiError, point, Servlet, servlet} from './servlet';

declare global {
    namespace Express {
        interface Request {
            user?: Model<any, any>;
        }
    }
}

export const getUser = async (request: Request): Promise<Model<any, any>> => {
    const auth = request.cookies.auth;
    const token = await M.Token.findOne({
        where: {
            token: auth,
        },
    });
    if (!token) {
        throw ApiError.Unauthorized;
    }
    return await token.getUser();
};

export const authHandler = async (request: Request, response: Response) => {
    request.user = await getUser(request);
};

@servlet('auth')
export class AuthServlet extends Servlet {
    private TokenExpires = 1000 * 60 * 60 * 24 * 30;

    @point('me', 'get')
    public async me(request: Request) {
        return await getUser(request);
    }

    @point('login', 'post')
    public async login(request: Request, response: Response) {
        const user = await M.User.findOne({
            where: {
                username: request.body.username,
                password: Octarine.instance.passwordHash(request.body.password),
            },
        });
        if (!user) {
            throw ApiError.BadRequest;
        }
        const token = await M.Token.create({
            token: crypto.randomBytes(20).toString('hex'),
            userId: user.id,
        });
        response.cookie('auth', token.token, {
            maxAge: this.TokenExpires,
        });
        return user;
    }

    @point('logout', 'post')
    public async logout(request: Request, response: Response) {
        response.clearCookie('auth');
        return 'ok';
    }
}
