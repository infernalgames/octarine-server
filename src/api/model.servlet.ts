import {Request} from 'express';
import {Model} from 'sequelize';
import * as sequelize from 'sequelize';
import {M} from '../database/bundle';
import {ApiError, point, Servlet} from './servlet';

export class ModelServlet extends Servlet {
    public modelName: string = '';
    public template = {};

    public get model(): Model<any, any> | any {
        return M[this.modelName];
    }

    public async modelToJSON(model: any) {
        return model.toJSON();
    }

    public buildIncludeFilter(key: string, value: any): any {
        return null;
    }

    public buildFilter(key: string, value: any) {
        if (key === 'id' || key.endsWith('Id')) {
            return {[key]: value};
        } else {
            return {[key]: {[sequelize.Op.like]: '%' + value + '%'}};
        }
    }

    @point('')
    public async get_list(request: Request) {
        const count = parseInt(request.query.count || 10, 10);
        const page = parseInt(request.query.page || 1, 10);
        const filters = [];
        const includes = [];
        if (request.query.filter) {
            for (const [key, value] of Object.entries(request.query.filter)) {
                if (this.model.attributes[`${key}Id`]) {
                    const include = this.buildIncludeFilter(key, value);
                    if (include) {
                        includes.push(include);
                    }
                } else {
                    const filter = this.buildFilter(key, value);
                    if (filter) {
                        filters.push(filter);
                    }
                }
            }
        }
        const where = filters.length ? {[sequelize.Op.or]: filters} : undefined;
        const total = await this.model.unscoped().findAll({
            attributes: [[sequelize.fn('COUNT', sequelize.col('*')), 'count']],
            where: where,
            include: includes,
        });
        const order = [];
        if (request.query.sort) {
            for (const [key, value] of Object.entries(request.query.sort)) {
                order.push([sequelize.col(key), value]);
            }
        }
        const data = await this.model.findAll({
            limit: count,
            offset: (page - 1) * count,
            where: where,
            order: order,
            include: includes,
        });
        return {
            data: await Promise.all(data.map(async (item: any) => await this.modelToJSON(item))),
            paginator: {count: count, page: page, total: total[0].get('count')},
        };
    }

    @point('new')
    public async get_new(request: Request) {
        return this.template;
    }

    @point(':id')
    public async get(request: Request) {
        const model = await this.model.findByPk(request.params.id);
        if (!model) {
            throw ApiError.NotFound;
        }
        return await this.modelToJSON(model);
    }

    @point('new', 'post')
    public async post_new(request: Request) {
        const data = request.body;
        data.id = null;
        const model = await this.model.create(data);
        return await this.modelToJSON(model);
    }

    @point(':id', 'post')
    public async post(request: Request) {
        const model = await this.model.findByPk(request.params.id);
        if (!model) {
            throw ApiError.NotFound;
        }
        await model.update(request.body);
        return await this.modelToJSON(model);
    }

    @point(':id', 'delete')
    public async delete(request: Request) {
        const model = await this.model.findByPk(request.params.id);
        if (!model) {
            throw ApiError.NotFound;
        }
        await model.destroy();
        const data = await this.modelToJSON(model);
        data.deleted = true;
        return data;
    }
}
