import {Express, Request, Response} from 'express';

export class ApiError extends Error {
    public static BadRequest = new ApiError(400, 'Bad Request');
    public static Unauthorized = new ApiError(401, 'Unauthorized');
    public static Forbidden = new ApiError(403, 'Forbidden');
    public static NotFound = new ApiError(404, 'Not Found');

    public name = 'ApiError';

    constructor(public code: number, public reason: string) {
        super();
    }
}

interface IPointDescriptor {
    url: string;
    method: string;
    target: any;
    name?: string;
    value: any;
    handler: any;
}

class Binder {
    public points: { [key: string]: IPointDescriptor[] } = {};
    public servletPoints: { [key: string]: IPointDescriptor[] } = {};

    public registerPoint(target: any, value: IPointDescriptor) {
        if (!this.points[target.name]) {
            this.points[target.name] = [];
        }
        this.points[target.name].push(value);
    }

    public registerServlet(target: any, api: string, handler: any) {
        if (!this.servletPoints[target.name]) {
            this.servletPoints[target.name] = [];
        }
        let current = target;
        let name = current.name;
        while (name) {
            if (this.points[name]) {
                for (const item of this.points[name]) {
                    const value: IPointDescriptor = {
                        url: `${api}/${item.url}`,
                        method: item.method,
                        target: new target(),  // ToDo
                        name: item.name,
                        value: item.value,
                        handler: handler,
                    };
                    this.servletPoints[target.name].push(value);
                }
            }
            current = Object.getPrototypeOf(current);
            name = current ? current.name : null;
        }
    }

    public bind(target: any, app: Express) {
        if (!this.servletPoints[target.name]) {
            return;
        }
        for (const item of this.servletPoints[target.name]) {
            // console.log(`[bind] [${item.method}] "${item.url}" on ${target.name}.${item.name}`);
            // @ts-ignore
            app[item.method](`/${item.url}`, async (request: Request, response: Response) => {
                try {
                    if (item.handler) {
                        await item.handler(request, response);
                    }
                    const data = await item.value.call(item.target, request, response);
                    if (data !== undefined) {
                        response.json({
                            code: 200,
                            response: data,
                        });
                    }
                } catch (e) {
                    if (/*e instanceof ApiError*/ e.name === ApiError.name) {
                        response.json({
                            code: e.code,
                            message: e.reason,
                        });
                    } else {
                        console.error(e);
                        response.json({
                            code: 503,
                            message: 'Internal Server Error',
                            error: e,
                        });
                    }
                }
            });
        }
    }
}

export const binder: Binder = new Binder();

export function point(url: string, method: string = 'get') {
    return (target: any, key: string, descriptor: TypedPropertyDescriptor<any>) => {
        binder.registerPoint(target.constructor, {
            url: `${url}`,
            method: method,
            name: key,
            target: null,
            value: descriptor.value,
            handler: null,
        });
        return descriptor;
    };
}

export function servlet(api: string, handler: any = null) {
    return (target: any) => {
        binder.registerServlet(target, api, handler);
        return target;
    };
}

export class Servlet {
    public static bind(app: Express) {
        binder.bind(this, app);
    }
}

export interface ServletClass<T extends Servlet> {
    new(...args: any[]): T;
    bind(app: Express): void;
}
