import * as crypto from 'crypto';
import {Express} from 'express';
import {Models} from 'sequelize';
import {AuthServlet} from './api/auth.servlet';
import {RoleServlet} from './api/role.servlet';
import {ServletClass} from './api/servlet';
import {UserServlet} from './api/user.servlet';
import {IConfig} from './config';
import {M} from './database/bundle';
import {Patch} from './database/common.db';
import {buildModels, createDatabase, ModelDefinition} from './database/core';
import {Translate, TranslateValue} from './database/translate.db';
import {Permission, PermissionValue, Role, Token, User} from './database/user.db';
import {createApp} from './server';

export class Octarine {
    public static instance: Octarine;

    public app: Express;
    public database: any;
    public models: Models;

    constructor(
        public config: IConfig,
        models?: Array<ModelDefinition>,
        servlets?: Array<ServletClass<any>>,
    ) {
        let allServlet: Array<ServletClass<any>> = [
            AuthServlet,
            UserServlet,
            RoleServlet,
        ];
        if (servlets) {
            allServlet = allServlet.concat(servlets);
        }
        this.app = createApp(allServlet);
        this.database = createDatabase(config.database);
        let allModels: Array<ModelDefinition> = [
            new Patch(),
            new PermissionValue(),
            new Permission(),
            new Role(),
            new User(),
            new Token(),
            new TranslateValue(),
            new Translate(),
        ];
        if (models) {
            allModels = allModels.concat(models);
        }
        this.models = buildModels(this.database, allModels);
        Object.assign(M, this.models);
        Octarine.instance = this;
    }

    public start(port: number) {
        this.app.listen(port);
    }

    public passwordHash(password: string): string {
        return crypto
            .createHash('md5')
            .update(`${this.config.salt}.${password}`)
            .digest('hex');
    }
}
